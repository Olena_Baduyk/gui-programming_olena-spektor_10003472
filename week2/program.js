function process() {

    var originalAmount = parseFloat(document.getElementById("original").value);
    var taxPercentage = parseFloat(document.getElementById("tax").value);
    
    var total = originalAmount * (taxPercentage / 100) + originalAmount;  
    
    document.getElementById("total").value = total;
}   

function reset() {
   var a = document.getElementById("original");
   a.value = "";

   var b = document.getElementById("tax");
   b.value = "";

   var c = document.getElementById("total");
   c.value = "";
}

