# **CODING JOURNAL**

## *My primary module is Electron.*

# **Week 1**

Links (about markdown documents)

[Syntax Rules (the most comfortable to use)](https://help.github.com/articles/basic-writing-and-formatting-syntax/)

[Documenting your projects on GitHub](https://guides.github.com/features/wikis/)

[Markdown Syntax](https://guides.github.com/pdfs/markdown-cheatsheet-online.pdf)

[Mastering Markdown](https://guides.github.com/features/mastering-markdown/)

[Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

**To start a brand new app**


From the Git Bash or Terminal type in the following commands:
```
cd ~/Desktop
git init electronapp
cd electronapp
touch main.js index.html package.json => to create files in the folder
code . => to start visual studio code
```
To run the app use command (the command can be done in the terminal  inside Visual Studio Code)
```
npm start
```

# **Week 2**




[Detailed instructions](https://www.christianengvall.se/electron-hello-world/)

**File sttructure**
Electron app needs three files:
1. package.json 
2. main.js
3. index.html

**Folder structure** should look like this:

1. index.html
2. main.js
3. package.json

**package.json**

To add content to package.json we need a name, a version and a main setting that points to a script that will handle the starting up of the app. If main is not present electron will look for a file called index.js.

```
{
    "name": "an-electron-form-application",
    "version": "1.0.0",
    "main": "main.js",
    "scripts" : {
        "start" : "electron ."
    },
    "devDependencies": {
    "electron": "~1.6.2"
  }
}
```
Have a look [here](https://docs.npmjs.com/files/package.json) for the devDependencies explanations.

**main.js**

Copy and paste the following code to the main.js file to make an electron app to open. This code is borrowed from [electron quick start.](https://github.com/electron/electron-quick-start)

```
const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({width: 800, height: 600})

  // and load the index.html of the app.
  mainWindow.loadURL(`file://${__dirname}/index.html`)

  // Open the DevTools.
  mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
```

**index.html**

Create the information that you want to be displayed as your gui.
(Use this code as a template)

```
<!DOCTYPE html>
<html>

  <head>
    <meta charset="UTF-8">
    <title>Electron Hello World!</title>
  </head>

  <body>

    <h1>Electron Hello World!</h1>
    
    We are using node <script>document.write(process.versions.node)</script>,
    Chromium <script>document.write(process.versions.chrome)</script>,
    and Electron <script>document.write(process.versions.electron)</script>.

  </body>

</html>
```

**To run and start an app**

First use command 
```
npm install
```
and then 
```
npm start
```
make sure you are in the correct folder
if the folder "node_modules" was created before - use only 
```
npm start
```

**.gitignore**

.gitignore file needs to be created in the folder with the application to prevent uploading all node modules to the repository. Inside the file write "node_modules". In this way you specify what files to ignore when you push the code.

To create .gitignore file using cmd, use command:
```
touch .gitignore
``` 


**Practice**

Task for week 2 [here](http://to-bcs.nz/COMP6001/week02-electron-task)

Application in the folder 'week2'

 In the index.html file I created a form, where I put 5 input tags. 3 of them are labels with input fields and 2 are buttons. 

3rd label is read only which made by 'readonly' property and set to 'readonly' value:
```
<input ... readonly="readonly" />
```

Buttons made with input tag where type is set to button:
```
<input type="button" ... />
```


In program.js file actions done with *onClick*